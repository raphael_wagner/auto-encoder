import os
import torch
import matplotlib.pyplot as plt
import numpy as np
import cv2
from custom_ae import AE
from PIL import Image
from glob import glob
from torchvision.transforms import ToPILImage

tensor_to_image = ToPILImage()
image_size = 256

device = 'cuda' if torch.cuda.is_available() else 'cpu'
dtype = torch.float32

cwd = os.getcwd()
if not os.path.exists(os.path.join(cwd, 'output')):
    os.mkdir(os.path.join(cwd, 'output'))

PATH = os.path.join(cwd, 'callback_ae/epoch_5000.pt')
state_dict = torch.load(PATH, map_location=torch.device('cpu'))
model = AE(image_size=image_size)
model.load_state_dict(state_dict)

model = model.to(device, dtype=dtype)

# get backgournd
# load image
train_image_path = os.path.join(cwd, 'change_train/images/IMG_7868.JPEG')


def preprocessing(image):
    rgb_planes = cv2.split(image)

    result_planes = []
    result_norm_planes = []
    for plane in rgb_planes:
        dilated_img = cv2.dilate(plane, np.ones((7, 7), np.uint8))
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(plane, bg_img)
        norm_img = cv2.normalize(diff_img, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        result_planes.append(diff_img)
        result_norm_planes.append(norm_img)

    result = cv2.merge(result_planes)
    result_norm = cv2.merge(result_norm_planes)

    return result


def load_image(image_path):
    image = np.array(Image.open(image_path))
    image = cv2.resize(image, (image_size, image_size))
    #image = preprocessing(image)
    image = image.transpose(2, 0, 1)  # MxNx3 -> 3xMxN
    image = np.ascontiguousarray(image) / 255
    image = torch.from_numpy(image)
    image = image.to(device, dtype=dtype)
    if image.ndimension() == 3:
        image = image.unsqueeze(0)
    return image



reference_image = load_image(train_image_path)
background = model(reference_image)

test_images = glob(os.path.join(cwd, 'change_test/images/*.JPEG'))

# train images
for count, path in enumerate(test_images):
    with torch.no_grad():
        x = load_image(path)
        x_hat = model(x)
        difference = torch.abs(x_hat - background)

        plt.imshow(x[0].permute(1, 2, 0).detach().numpy())
        plt.axis('off')
        plt.savefig(cwd + '/output/original' + str(count) + '.png')

        plt.gca()
        plt.gcf()
        plt.close()
        plt.imshow(x_hat[0].permute(1, 2, 0).detach().numpy())
        plt.axis('off')
        plt.savefig(cwd + '/output/recon' + str(count) + '.png')

        for i in range(3):
            plt.gca()
            plt.gcf()
            plt.close()
            plt.imshow(difference[0].permute(1, 2, 0).detach().numpy()[:, :, i], cmap='hot',
                       interpolation='nearest')
            plt.axis('off')
            plt.savefig(cwd + '/output/channel_' + str(count) + str(i) + '.png')

        mask = np.mean(np.array(difference[0].permute(1, 2, 0).detach().numpy()), axis=2)
        _, mask = cv2.threshold(mask, 0.085, 255, cv2.THRESH_BINARY)
        plt.gca()
        plt.gcf()
        plt.close()
        plt.imshow(mask)
        plt.axis('off')
        plt.savefig(cwd + '/output/mask' + str(count) + '.png')

        contours, _ = cv2.findContours(np.array(mask.copy(), dtype=np.uint8), 1,
                                       1)  # not copying here will throw an error
        rect2 = np.array(Image.open(path))
        scaling = 640. / image_size
        for k in range(len(contours)):
            rect = cv2.minAreaRect(contours[k])  # basically you can feed this rect into your classifier
            (x, y), (w, h), a = rect  # a - angle

            box = cv2.boxPoints(rect)
            box = np.int0(box)  # turn into ints

            x1 = int(np.min(box[:, 0]) * scaling)
            y1 = int(np.min(box[:, 1]) * scaling)
            x2 = int(np.max(box[:, 0]) * scaling)
            y2 = int(np.max(box[:, 1]) * scaling)

            rect2 = cv2.rectangle(rect2, (x1, y1), (x2, y2), (0, 0, 255), 2)

        plt.gca()
        plt.gcf()
        plt.close()
        plt.imshow(rect2)
        plt.axis('off')
        plt.savefig(cwd + '/output/box' + str(count) + '.png')

        if count > 20:
            break
