import torch.nn.functional as F
from torch import nn
import torch



class ConvEncoder(nn.Module):
    def __init__(self):
        super(ConvEncoder, self).__init__()

        # Encoder
        self.conv1 = nn.Conv2d(3, 16, stride=2, kernel_size=4, padding=0)
        self.conv2 = nn.Conv2d(16, 32, stride=2, kernel_size=4, padding=0)
        self.conv3 = nn.Conv2d(32, 64, stride=2, kernel_size=4, padding=0)

    def forward(self, x):
        # print('ENCODER')
        x = F.gelu(self.conv1(x))
        # print('conv_1', x.shape)
        x = F.gelu(self.conv2(x))
        # print('conv_2', x.shape)
        x = F.gelu(self.conv3(x))
        # print('conv_3', x.shape)
        return x


class ConvDecoder(nn.Module):
    def __init__(self):
        super(ConvDecoder, self).__init__()

        # Decoder
        self.conv1 = nn.ConvTranspose2d(64, 32, stride=2, kernel_size=4, padding=0)
        self.conv2 = nn.ConvTranspose2d(32, 16, stride=2, kernel_size=4, output_padding=1)
        self.conv3 = nn.ConvTranspose2d(16, 3, stride=2, kernel_size=4, padding=0)

    def forward(self, x):
        # print('DECODER')
        x = F.gelu(self.conv1(x))
        # print('conv_1', x.shape)
        x = F.gelu(self.conv2(x))
        # print('conv_2', x.shape)
        x = torch.sigmoid(self.conv3(x))
        # print('conv_3', x.shape)
        return x


class AE(nn.Module):
    def __init__(self, image_size=128):
        super().__init__()

        # encoder, decoder
        self.encoder = ConvEncoder()
        self.decoder = ConvDecoder()

        with torch.no_grad():
            dummy_input = torch.randn(size=[1, 3, image_size, image_size])

            enc_out = self.encoder(dummy_input)
            print('INPUT DIM', image_size * image_size * 3)
            print('USING ENCODE DIM', enc_out.view(1, -1).shape[-1])
            decoded = self.decoder(enc_out)
            print('DECODED IMGSIZE', decoded.shape[1], decoded.shape[2], decoded.shape[3])

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=1e-4)

    def forward(self, x):
        # encode x to get the mu and variance parameters
        x_encoded = self.encoder(x)
        x_hat = self.decoder(x_encoded)

        return x_hat
