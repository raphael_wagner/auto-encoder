from custom_ae import AE
import cv2
import torch
import os
from PIL import Image
import numpy as np
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
from tqdm import tqdm
from torch.nn import MSELoss
import math

device = 'cuda' if torch.cuda.is_available() else 'cpu'
dtype = torch.float32
cwd = os.getcwd()
save_dir = os.path.join(cwd, 'callback_ae')
if not os.path.exists(save_dir):
    os.mkdir(save_dir)

image_size = 256
model = AE(image_size=image_size)
weights = torch.load(os.path.join(cwd, 'base_ae_.pt'), map_location='cpu')
# model.load_state_dict(weights)
model = model.to(device, dtype=dtype)

epochs = 5000
optimizer = optim.Adam(model.parameters(), lr=1e-3, weight_decay=0.0005)
lf = lambda x: (((1 + math.cos(x * math.pi / (epochs / 2))) / 2) ** 1.0) * (1 - 2e-2) + 2e-2  # cosine
scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lf)

loss_fn = MSELoss()

# load image
train_image_path = os.path.join(cwd, 'change_train/images/IMG_6339.JPG')
image = np.array(Image.open(train_image_path))
image = cv2.resize(image, (image_size, image_size))
image = image.transpose(2, 0, 1)  # MxNx3 -> 3xMxN
image = np.ascontiguousarray(image) / 255
image = torch.from_numpy(image)
image = image.to(device, dtype=dtype)
if image.ndimension() == 3:
    image = image.unsqueeze(0)

pbar = tqdm(range(1, epochs + 1))
optimizer.zero_grad()
for i in pbar:

    x_hat = model(image)
    loss = loss_fn(x_hat.view(1, -1), image.view(1, -1))
    loss.backward()
    optimizer.step()

    optimizer.zero_grad()

    scheduler.step()
    if i % 500 == 0:
        torch.save(model.state_dict(), os.path.join(save_dir, 'epoch_{}.pt'.format(str(i))))

    mem = '%.3gG' % (torch.cuda.memory_reserved() / 1E9 if torch.cuda.is_available() else 0)  # (GB)
    s = ('%10s' * 2 + '%10.4g' * 1) % (
        '%g/%g' % (i, epochs), mem, loss)
    pbar.set_description(s)
